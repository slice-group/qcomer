Rails.application.routes.draw do

  root to: 'frontend#index'

  get "/establishments/category/:permalink", to: "frontend#search_by_category", as: "search_by_category"

  get "/establishments", to: "frontend#establishments", as: "frontend_establishment"

  get "/establishments/:permalink", to: "frontend#establishment_profile", as: "frontend_establishment_profile"
  
  devise_for :users, skip: [:registration]

  mount InyxContactUsRails::Engine, :at => '', as: 'messages'

  resources :admin, only: [:index]

  scope :admin do
  	resources :users do
  		collection do
	  		post '/delete', to: 'users#delete'
  		end
  	end

    resources :categories do
      collection do
        post '/delete', to: 'categories#delete'
      end
    end

    resources :establishments do
      resources :dishes do
        collection do
            post '/delete', to: 'dishes#delete'
          end
      end
      collection do
        post '/delete', to: 'establishments#delete'
      end
    end


    resources :services do
      collection do
        post '/delete', to: 'services#delete'
      end
    end

    resources :orders, except: [:new, :create, :destroy]

  end


  scope :api do
    #api categories
    get '/categories', to: 'api#categories'

    #api establishments
    get '/establishments', to: 'api#establishments'
    get '/establishments/:permalink', to: 'api#establishment_perfil'
    get '/establishments/:permalink/dishes', to: 'api#dishes_the_establishment'

    #api search
    get '/search', to: 'api#search'

    #api authentication
    post '/authenticate', to: 'authentication#login'
    post '/authenticate/sign_up', to: 'authentication#register'
    delete '/authenticate/sign_out', to: 'authentication#destroy'
    post '/authenticate/forgot-password', to: 'authentication#forgot_password'
    post 'authenticate/password/edit', to: 'authentication#reset_password'

    #api users
    put 'settings/edit', to: 'api_user#edit'
    put 'settings/password', to: 'api_user#edit_password'

    #api orders
    post 'orders/new', to: 'api#create_order'
    get 'orders/:id/canceled', to: "api#canceled_order"
    get 'orders', to: 'api#index_orders'
    get 'orders/:id/pay', to: 'api#pay_order'

    #api mercadopago notifications
    post 'payment/notifications', to: 'api#payment_notifications'
    post 'payment/notifications/ipn', to: 'api#payment_notifications_mp'
  end
  
end
