# Agregar datos de configuración
InyxContactUsRails.setup do |config|
	config.mailer_to = "gbrlmrllo@gmail.com"
	config.mailer_from = "no-reply@qcomerenparaguana.com"
	config.name_web = "Qcomerenparaguana"
	# Activar o Desactivar la ruta especificada ("messages/new")
	config.messages_new = false
	#Route redirection after send
	config.redirection = "/#contactos"


	# Agregar keys de google recaptcha
	Recaptcha.configure do |config|
	  config.public_key  = "6LdpnQMTAAAAAJMKVyHLNkT6WDTosBEFQJg7BV1w"
	  config.private_key = "6LdpnQMTAAAAAOHckt0hDDaQUrLGZoRHtnRv6aUe"
	end
end