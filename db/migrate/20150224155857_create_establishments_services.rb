class CreateEstablishmentsServices < ActiveRecord::Migration
  def change
    create_table :establishments_services do |t|
      t.belongs_to :establishment, index: true
      t.belongs_to :service, index: true

      t.timestamps
    end
  end
end
