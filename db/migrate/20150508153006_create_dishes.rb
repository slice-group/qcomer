class CreateDishes < ActiveRecord::Migration
  def change
    create_table :dishes do |t|
      t.string :name
      t.text :description
      t.float :price
      t.string :delivery_time
      t.string :image
      t.string :permalink
      t.belongs_to :establishment

      t.timestamps null: false
    end
  end
end
