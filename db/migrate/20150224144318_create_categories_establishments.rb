class CreateCategoriesEstablishments < ActiveRecord::Migration
  def change
    create_table :categories_establishments do |t|
      t.belongs_to :establishment, index: true
      t.belongs_to :category, index: true

      t.timestamps
    end
  end
end
