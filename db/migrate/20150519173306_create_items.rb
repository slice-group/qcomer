class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.belongs_to :order, index: true	
      t.belongs_to :dish, index: true
      t.integer :quantity
      t.text :observation
      t.string :name
      t.float :price
    end
  end
end
