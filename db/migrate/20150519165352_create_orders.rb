class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.integer :status_id
      t.text :address
      t.float :price_delivery
      t.integer :payment_id
      t.text :observation
      t.belongs_to :payment_reference

      t.timestamps null: false
    end
  end
end
