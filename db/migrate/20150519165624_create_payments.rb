class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :name
      t.string :collection_status
      t.string :date_created
      t.string :preference_id
      t.string :init_point
      t.string :sandbox_init_point

      t.timestamps null: false
    end
  end
end
