class CreatePaymentReferences < ActiveRecord::Migration
  def change
    create_table :payment_references do |t|
      t.string :collection_status
      t.string :collection_id
      t.string :preference_id
      t.string :date_created
      t.string :init_point
      t.string :sandbox_init_point

      t.timestamps null: false
    end
  end
end
