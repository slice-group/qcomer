class CreatePaymentStatuses < ActiveRecord::Migration
  def change
    create_table :payment_statuses do |t|
      t.string :payment_id
      t.string :date_created
      t.string :date_approved
      t.string :money_release_date
      t.string :reason
      t.string :transaction_amount
      t.string :total_paid_amount
      t.string :net_received_amount
      t.string :status
      t.string :status_detail
      t.integer :order_id

      t.timestamps null: false
    end
  end
end
