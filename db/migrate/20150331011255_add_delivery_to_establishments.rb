class AddDeliveryToEstablishments < ActiveRecord::Migration
  def change
    add_column :establishments, :delivery, :boolean
  end
end
