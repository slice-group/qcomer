class CreateCategoriesDishes < ActiveRecord::Migration
  def change
    create_table :categories_dishes do |t|
      t.belongs_to :dish, index: true
      t.belongs_to :category, index: true

      t.timestamps null: false
    end
    #add_foreign_key :categories_dishes, :dishes
    #add_foreign_key :categories_dishes, :categories
  end
end
