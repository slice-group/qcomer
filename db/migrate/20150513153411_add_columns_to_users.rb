class AddColumnsToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :identification, :string
  	add_column :users, :address, :text
  	add_column :users, :phone, :string
  	add_column :users, :mobile, :string
  end
end
