class AddScheduleToEstablishments < ActiveRecord::Migration
  def change
  	add_column :establishments, :day_aperture, :string
  	add_column :establishments, :day_close, :string
  	add_column :establishments, :hour_aperture, :string
  	add_column :establishments, :hour_close, :string
  end
end
