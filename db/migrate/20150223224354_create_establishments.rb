class CreateEstablishments < ActiveRecord::Migration
  def change
    create_table :establishments do |t|
      t.string :brand
      t.string :banner
      t.string :name
      t.text :description
      t.string :permalink
      t.boolean :public
      t.text :address
      t.string :city
      t.string :state
      t.string :country
      t.string :phone_1
      t.string :phone_2
      t.string :facebook_name
      t.string :instagram_name
      t.string :twitter_name
      t.string :latitude
      t.string :longitude
      t.boolean :credit_card
      t.boolean :debit
      t.boolean :cash

      t.timestamps
    end
  end
end
