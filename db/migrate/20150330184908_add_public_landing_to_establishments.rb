class AddPublicLandingToEstablishments < ActiveRecord::Migration
  def change
    add_column :establishments, :public_landing, :boolean
  end
end
