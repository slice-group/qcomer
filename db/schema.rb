# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150622193747) do

  create_table "categories", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.string   "permalink",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories_dishes", force: :cascade do |t|
    t.integer  "dish_id",     limit: 4
    t.integer  "category_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "categories_dishes", ["category_id"], name: "index_categories_dishes_on_category_id", using: :btree
  add_index "categories_dishes", ["dish_id"], name: "index_categories_dishes_on_dish_id", using: :btree

  create_table "categories_establishments", force: :cascade do |t|
    t.integer  "establishment_id", limit: 4
    t.integer  "category_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "categories_establishments", ["category_id"], name: "index_categories_establishments_on_category_id", using: :btree
  add_index "categories_establishments", ["establishment_id"], name: "index_categories_establishments_on_establishment_id", using: :btree

  create_table "dishes", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.text     "description",      limit: 65535
    t.float    "price",            limit: 24
    t.string   "delivery_time",    limit: 255
    t.string   "image",            limit: 255
    t.string   "permalink",        limit: 255
    t.integer  "establishment_id", limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "establishments", force: :cascade do |t|
    t.string   "brand",          limit: 255
    t.string   "banner",         limit: 255
    t.string   "name",           limit: 255
    t.text     "description",    limit: 65535
    t.string   "permalink",      limit: 255
    t.boolean  "public",         limit: 1
    t.text     "address",        limit: 65535
    t.string   "city",           limit: 255
    t.string   "state",          limit: 255
    t.string   "country",        limit: 255
    t.string   "phone_1",        limit: 255
    t.string   "phone_2",        limit: 255
    t.string   "facebook_name",  limit: 255
    t.string   "instagram_name", limit: 255
    t.string   "twitter_name",   limit: 255
    t.string   "latitude",       limit: 255
    t.string   "longitude",      limit: 255
    t.boolean  "credit_card",    limit: 1
    t.boolean  "debit",          limit: 1
    t.boolean  "cash",           limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "day_aperture",   limit: 255
    t.string   "day_close",      limit: 255
    t.string   "hour_aperture",  limit: 255
    t.string   "hour_close",     limit: 255
    t.boolean  "public_landing", limit: 1
    t.boolean  "delivery",       limit: 1
  end

  create_table "establishments_services", force: :cascade do |t|
    t.integer  "establishment_id", limit: 4
    t.integer  "service_id",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "establishments_services", ["establishment_id"], name: "index_establishments_services_on_establishment_id", using: :btree
  add_index "establishments_services", ["service_id"], name: "index_establishments_services_on_service_id", using: :btree

  create_table "inyx_contact_us_rails_messages", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "subject",    limit: 255
    t.string   "email",      limit: 255
    t.text     "content",    limit: 65535
    t.boolean  "read",       limit: 1,     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "items", force: :cascade do |t|
    t.integer "order_id",    limit: 4
    t.integer "dish_id",     limit: 4
    t.integer "quantity",    limit: 4
    t.text    "observation", limit: 65535
    t.string  "name",        limit: 255
    t.float   "price",       limit: 24
  end

  add_index "items", ["dish_id"], name: "index_items_on_dish_id", using: :btree
  add_index "items", ["order_id"], name: "index_items_on_order_id", using: :btree

  create_table "locations", force: :cascade do |t|
    t.string   "ip",           limit: 255
    t.string   "country_code", limit: 255
    t.string   "country",      limit: 255
    t.float    "latitude",     limit: 24
    t.float    "longitude",    limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "user_id",              limit: 4
    t.integer  "status_id",            limit: 4
    t.text     "address",              limit: 65535
    t.float    "price_delivery",       limit: 24
    t.integer  "payment_id",           limit: 4
    t.text     "observation",          limit: 65535
    t.integer  "payment_reference_id", limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "payment_references", force: :cascade do |t|
    t.string   "collection_status",  limit: 255
    t.string   "collection_id",      limit: 255
    t.string   "preference_id",      limit: 255
    t.string   "date_created",       limit: 255
    t.string   "init_point",         limit: 255
    t.string   "sandbox_init_point", limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "payment_statuses", force: :cascade do |t|
    t.string   "payment_id",          limit: 255
    t.string   "date_created",        limit: 255
    t.string   "date_approved",       limit: 255
    t.string   "money_release_date",  limit: 255
    t.string   "reason",              limit: 255
    t.string   "transaction_amount",  limit: 255
    t.string   "total_paid_amount",   limit: 255
    t.string   "net_received_amount", limit: 255
    t.string   "status",              limit: 255
    t.string   "status_detail",       limit: 255
    t.integer  "order_id",            limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "payments", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.string   "collection_status",  limit: 255
    t.string   "date_created",       limit: 255
    t.string   "preference_id",      limit: 255
    t.string   "init_point",         limit: 255
    t.string   "sandbox_init_point", limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.string   "permalink",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "statuses", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255,   default: "", null: false
    t.string   "encrypted_password",     limit: 255,   default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                   limit: 255
    t.string   "permalink",              limit: 255
    t.string   "authentication_token",   limit: 255
    t.string   "identification",         limit: 255
    t.text     "address",                limit: 65535
    t.string   "phone",                  limit: 255
    t.string   "mobile",                 limit: 255
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
