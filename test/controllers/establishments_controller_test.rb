require 'test_helper'

class EstablishmentsControllerTest < ActionController::TestCase
  setup do
    @establishment = establishments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:establishments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create establishment" do
    assert_difference('Establishment.count') do
      post :create, establishment: { address: @establishment.address, banner: @establishment.banner, brand: @establishment.brand, city: @establishment.city, country: @establishment.country, description: @establishment.description, facebook_name: @establishment.facebook_name, instagram_name: @establishment.instagram_name, latitude: @establishment.latitude, longitude: @establishment.longitude, name: @establishment.name, permalink: @establishment.permalink, phone_1: @establishment.phone_1, phone_2: @establishment.phone_2, public: @establishment.public, state: @establishment.state, twitter_name: @establishment.twitter_name }
    end

    assert_redirected_to establishment_path(assigns(:establishment))
  end

  test "should show establishment" do
    get :show, id: @establishment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @establishment
    assert_response :success
  end

  test "should update establishment" do
    patch :update, id: @establishment, establishment: { address: @establishment.address, banner: @establishment.banner, brand: @establishment.brand, city: @establishment.city, country: @establishment.country, description: @establishment.description, facebook_name: @establishment.facebook_name, instagram_name: @establishment.instagram_name, latitude: @establishment.latitude, longitude: @establishment.longitude, name: @establishment.name, permalink: @establishment.permalink, phone_1: @establishment.phone_1, phone_2: @establishment.phone_2, public: @establishment.public, state: @establishment.state, twitter_name: @establishment.twitter_name }
    assert_redirected_to establishment_path(assigns(:establishment))
  end

  test "should destroy establishment" do
    assert_difference('Establishment.count', -1) do
      delete :destroy, id: @establishment
    end

    assert_redirected_to establishments_path
  end
end
