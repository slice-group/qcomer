require 'elasticsearch/model'
class Establishment < ActiveRecord::Base
	include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
	has_and_belongs_to_many :categories
	has_and_belongs_to_many :services
  has_many :dishes, :dependent => :destroy
	before_save :create_permalink, on: :create
  validates_presence_of :name, :description, :address, :brand, :banner, :day_aperture, :day_close, :hour_aperture, :hour_close, :city, :state, :country
	validates_uniqueness_of :name, :case_sensitive => false
  validates :phone_1, numericality: { only_integer: true, length: 13 }, :allow_blank => true
  validates :description, length: { maximum: 220 }
  validate :method_payment?, :permit_public_landing?
  mount_uploader :brand, ImageUploader
	mount_uploader :banner, ImageUploader

	def as_json(options = {})
		{
			id: self.id,
			name: self.name,
      brand: self.brand.url,
      banner: self.banner.url,
			description: self.description,
			permalink: self.permalink,
			public: self.public ? "Publicado" : "No publicado",
			address: self.address,
			city: self.city,
			state: self.state,
			country: self.country,
			phone_1: self.phone_1,
      phone_2: self.phone_2,
      credit_card: self.credit_card ? 'Acepta tarjeta de credito' : 'No acepta tarjeta de credito',
      debit: self.debit ? 'Acepta debito' : 'No acepta debito',
      cash: self.cash ? 'Acepta efectivo' : 'No acepta efectivo',
      day_aperture: self.day_aperture,
      day_close: self.day_close,
      longitude: self.longitude,
      latitude: self.latitude,
      hour_aperture: self.hour_aperture,
      hour_close: self.hour_close,
      public_landing: self.public_landing ? 'Publicado en landing' : "No publicado en landing",
      facebook_name: self.facebook_name,
      twitter_name: self.twitter_name,
      instagram_name: self.instagram_name,
      delivery: self.delivery ? 'Aplica delivery' : 'No aplica delivery',
			categories: self.categories.map { |category| category.name } .join(", "),
			services: self.services.map { |category| category.name } .join(", "),
      open: (self.day_open? and self.hour_open?)
    }
	end

	def self.query(query)
    { query: { multi_match:  { query: query, fields: [:name, :description, :city, :state, :country, :categories, :services, :address] , operator: :and }  }, sort: { id: "desc" }, size: Establishment.count }
  end

  def self.query_frontend(query)
    { query: { multi_match:  { query: query, fields: [:name, :description, :city, :state, :country, :categories, :services, :address] , operator: :or }  }, sort: { id: "desc" }, size: Establishment.count }
  end

  def create_permalink
    self.permalink = self.name.downcase.parameterize
  end

  #params[:search] is false
  def self.index(current_user)
    Establishment.all.order("created_at DESC")
  end

  #params[:search] is true
  def self.index_search(objects, current_user)
    objects.all.order("created_at DESC")
  end

  #params[:search] is true
  def self.index_total(objects, current_user)
    objects.all.count
  end

  def self.route_index
    "/admin/establishments"
  end

  def index_elasticsearch_document
    self.__elasticsearch__.index_document
  end

  def self.multiple_destroy(ids, current_user)
    Establishment.destroy ids
  end

  def method_payment?
    errors.add(:base, "Debe seleccionar al menos un metodo de pago.") unless self.credit_card or self.debit or self.cash
  end

  def permit_public_landing?
    errors.add(:public_landing, "El limite de establecimientos publicados en su landing es de 12, no puede publicar este hasta que no desabilite algun otro.") if Establishment.where(public_landing: true).count > 12
  end

  def open
    self.day_open? and self.hour_open?
  end

  def day_open?
    days = { 0 => "Lunes", 1 => "Martes", 2 => "Miercoles", 3 => "Jueves", 4 => "Viernes", 5 => "Sabado", 6 => "Domingo"}
    range_aperture = (days.key(self.day_aperture)..days.key(self.day_close))
    range_aperture.include? Date.today.days_to_week_start
  end

  def hour_open?
    hours = {}
    (1..12).each_with_index { |x, y| hours.merge!({ "#{x}:00 AM" => x != 12 ? y+1 : 0})  } #hash_pm
    (1..12).each_with_index { |x, y| hours.merge!({ "#{x}:00 PM" =>  y+13})  } #hash_am
    range_aperture = (hours[self.hour_aperture]..hours[self.hour_close])

    if range_aperture.include? Time.now.hour and range_aperture.last != Time.now.hour
      true
    elsif range_aperture.include? Time.now.hour and range_aperture.last == Time.now.hour
      false
    end
  end



  settings analysis: {
    :analyzer => {
      :spanish_snowball => {
        :type => "snowball",
        :language => "Spanish",
        :filter => %w{asciifolding lowercase}
      }
    }
  } do
    mapping do
      [:name, :state, :description, :country, :city].each do |attribute|
        indexes attribute, type: 'string', analyzer: 'spanish_snowball'
      end
    end
  end

end

Establishment.__elasticsearch__.create_index! force: true
Establishment.import