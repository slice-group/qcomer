require 'elasticsearch/model'
class User < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  before_save :create_permalink, on: :create
  #before_save :index_elasticsearch_document, on: :update
  acts_as_token_authenticatable
  rolify
  validates_presence_of :name, :role_ids
  before_save :fill_atributes
  #has_many :posts, dependent: :destroy relation posts
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

    def as_json(options = {})
      {
        id: self.id,
        identification: self.identification,
        address: self.address,
        phone: self.phone,
        mobile: self.mobile,
        role_name: role_name,
        rol: self.roles.first.id,
        email: self.email,
        name: self.name,
        permalink: self.permalink,
        created_at: self.created_at,
        sign_in_count: self.sign_in_count,
        authentication_token: self.authentication_token,
        reset_password_token: self.reset_password_token
      }
    end

  def orders
    Order.where user_id: self.id
  end

  def role_name
    self.roles.first.name
  end
  
  def self.group_by_roles
    rtrn = {}
    [:admin, :moderator, :redactor].each do |rol|
      rtrn[rol] = User.with_role(rol).count
    end
    rtrn
  end

  def self.get_id(permalink)
    User.find_by_permalink(permalink).id
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:role_name, :name, :email, :permalink] , operator: :and }  }, sort: { id: "desc" }, size: User.count }
  end

  #params[:search] is false
  def self.index(current_user)
    User.where("id != #{current_user.id}").order("created_at DESC")
  end

  #params[:search] is true
  def self.index_search(objects, current_user)
    objects.where("id != #{current_user.id}").order("created_at DESC")
  end

  #params[:search] is true
  def self.index_total(objects, current_user)
    objects.where("id != #{current_user.id}").count
  end

  def self.route_index
    "/admin/users"
  end

  def index_elasticsearch_document
    self.__elasticsearch__.index_document
  end

  def create_permalink
    self.permalink = self.name.downcase.parameterize+"-"+SecureRandom.hex(4)
  end

  def self.multiple_destroy(ids, current_user)
    ids.each do |id|
      raise CanCan::AccessDenied.new("Access Denied", :delete, User) if id.to_i == current_user.id
    end
    User.destroy ids
  end

  #reset password
  def reset_password(new_password, new_password_confirmation)
    self.password = new_password
    self.password_confirmation = new_password_confirmation

    if valid?
      clear_reset_password_token
    end
    self.save
  end

  #send_reset_password
  def api_send_reset_password_instructions
    api_set_reset_password_token
    AuthenticationMailer.reset_password_instructions(self).deliver_now
  end

  protected

  def api_set_reset_password_token
    raw, enc = Devise.token_generator.generate(self.class, :reset_password_token)

    self.reset_password_token   = enc
    self.reset_password_sent_at = Time.now.utc
    self.save(validate: false)
    raw
  end

  def api_clear_reset_password_token
    self.reset_password_token = nil
    self.reset_password_sent_at = nil
  end

  def fill_atributes
    self.address, self.phone, self.identification, self.mobile = "", "", "", "" unless self.created_at? 
  end  
    
end

User.import