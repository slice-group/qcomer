class CategoriesDish < ActiveRecord::Base
  belongs_to :dish
  belongs_to :category
end
