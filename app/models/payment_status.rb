class PaymentStatus < ActiveRecord::Base
	belongs_to :order
	before_save :add_order_id
	after_save :changed_order_status_to_confirming
	validates_presence_of :payment_id
	
	def self.create_payment_status(params)
		attributes = PayMercadoPago::MP.get_payment(params)
		PaymentStatus.create params_payment_status(attributes)
	end

	private

	def add_order_id
		order = Order.find(self.reason.split("#").last.to_i) if !self.order_id
		self.order_id = order.id if !self.created_at? and !self.order_id
	end

	def changed_order_status_to_confirming
		if self.order.has_payment? :credito and !self.order.permit_pay?
			self.order.update_attributes status_id: 3
		end
	end

	def self.params_payment_status(attributes)
		attributes = attributes["response"]["collection"].as_json(only: ["id", "date_created", "date_approved", "money_release_date", "reason", "transaction_amount", "total_paid_amount", "net_received_amount", "status", "status_detail"])
		attributes["payment_id"] = attributes.delete("id")
		attributes
	end

end
