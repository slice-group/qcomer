class Item < ActiveRecord::Base
	belongs_to :order
	belongs_to :dish

	validates_presence_of :dish_id
	validates :quantity, numericality: { only_integer: true }
	before_save :add_name_and_price_dish


	def price_total
		self.price*self.quantity
	end

	def add_name_and_price_dish
		self.price = self.dish.price
		self.name = self.dish.name
	end
end
