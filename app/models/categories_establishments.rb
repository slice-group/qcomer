class CategoriesEstablishments < ActiveRecord::Base
  belongs_to :establishment
  belongs_to :category
end
