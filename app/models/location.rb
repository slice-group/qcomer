class Location < ActiveRecord::Base
	validate :ip_per_date

	def self.add(remote_ip)
		location = Geocoder.search(remote_ip).first
		unless location.nil?
			Location.create ip: remote_ip, country_code: location.country_code, country: location.country, latitude: location.latitude, longitude: location.longitude
		end
	end
	
	private
	
	def ip_per_date
		if Location.where(ip: self.ip).count > 0 && Location.where(ip: self.ip).order("created_at DESC").first.created_at.to_date == DateTime.now.to_date
			errors.add(:ip, 'ip validada')
		end
	end

end
