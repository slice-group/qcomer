require 'elasticsearch/model'
class Dish < ActiveRecord::Base	
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  has_and_belongs_to_many :categories
  belongs_to :establishment
  before_save :create_permalink
  validates :description, length: { maximum: 220 }
  has_many :items
  has_many :orders, :through => :items
  validates_presence_of :name, :description
  validates_numericality_of :price
  validates_numericality_of :delivery_time, :only_integer => true
  mount_uploader :image, ImageUploader
  

	def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name: self.name,
      description: self.description,
      price: self.price.to_s,
      establishment: self.establishment.name,
      delivery_time: self.delivery_time.to_s,
      categories: self.categories.map { |category| category.name } .join(", ")
    }.as_json
  end

  def create_permalink
    self.permalink=self.name.downcase.parameterize
  end

	def self.query(query)
    { query: { multi_match:  { query: query, fields: [:name, :description, :price, :delivery_time] , operator: :and }  }, sort: { id: "desc" }, size: Dish.count }
  end

  def self.query_frontend(query, sort)
    query = { query: { multi_match:  { query: query, fields: [:name, :description, :price, :delivery_time, :establishment, :categories] , operator: :or }  }, sort: { }, size: Dish.count }
    query[:sort][:price] = sort if !sort.blank? #ordernar pro priceios 'sort' = asc or desc
    query
  end

  def create_permalink
    self.permalink = self.name.downcase.parameterize
  end

  #params[:search] is false
  def self.index(current_user, establishment_id)
    Dish.where(establishment_id: establishment_id).order("created_at DESC")
  end

  #params[:search] is true
  def self.index_search(objects, current_user, establishment_id)
    objects.where(establishment_id: establishment_id).order("created_at DESC")
  end

  #params[:search] is true
  def self.index_total(objects, current_user, establishment_id)
    objects.where(establishment_id: establishment_id).count
  end

  def self.route_index(establishment_id)
    "/admin/establishments/#{establishment_id}/dishes"
  end

  def index_elasticsearch_document
    self.__elasticsearch__.index_document
  end

  def self.multiple_destroy(ids, current_user)
    Dish.destroy ids
  end

end

Dish.import

