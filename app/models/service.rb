require 'elasticsearch/model'
class Service < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
	has_and_belongs_to_many :establishments
	validates_presence_of :name, :description

	def as_json(options = {})
		{
			id: self.id,
			name: self.name,
			description: self.description
		}
	end

	def self.query(query)
    { query: { multi_match:  { query: query, fields: [:name, :description] , operator: :and }  }, sort: { id: "desc" }, size: Service.count }
  end

   #params[:search] is false
  def self.index(current_user)
    Service.all.order("created_at DESC")
  end

  #params[:search] is true
  def self.index_search(objects, current_user)
    objects.all.order("created_at DESC")
  end

  #params[:search] is true
  def self.index_total(objects, current_user)
    objects.all.count
  end

  def self.route_index
    "/admin/categories"
  end

  def index_elasticsearch_document
    self.__elasticsearch__.index_document
  end

  def self.multiple_destroy(ids, current_user)
    Service.destroy ids
  end
  
end

#Service.import