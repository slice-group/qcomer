require 'elasticsearch/model'
class Order < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
	has_many :items, dependent: :destroy
  accepts_nested_attributes_for :items, :reject_if => :all_blank, :allow_destroy => true
	has_many :dishes, :through => :items
	belongs_to :user
  belongs_to :status
	belongs_to :payment
  belongs_to :payment_reference
  has_many :payment_statuses
  before_update :index_elasticsearch_document
  validates_presence_of :address, :payment_id
  validates_presence_of :status_id, :on=>:update
  validates :price_delivery, numericality: { :greater_than => 0 }, on: :update, :if => lambda { |a| a[:status_id] != 6 }

  before_save :assign_status, :assign_price_delivery, :create_preference
  after_destroy :delete_items

  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      address: self.address,
      client: self.user.name,
      cedula: self.user.identification,
      status: self.status.name
    }.as_json
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:id, :address, :client, :cedula, :status], operator: :and }  }, sort: { id: "desc" }, size: Order.count }
  end

  def items_count
    self.items.count
  end

  #suma de precio de todos los platos de la orden
  def price_dishes_total
    self.items.inject(0) { |price, item| price+(item.price*item.quantity) } .round 2
  end

  #precio total de la orden
  def price_order_total
    self.price_delivery.to_i+self.price_dishes_total
  end

  def has_status? name
    name.to_s == self.status.name
  end

  def has_payment? name
    name.to_s == self.payment.name
  end

	#params[:search] is false
  def self.index(current_user)
    Order.all.order("id DESC")
  end

  #params[:search] is true
  def self.index_search(objects, current_user)
    objects.all.order("id DESC")
  end

  #params[:search] is true
  def self.index_total(objects, current_user)
    objects.all.count
  end

  def self.route_index
    "/admin/orders"
  end

  def index_elasticsearch_document
    self.__elasticsearch__.index_document
  end

  def self.multiple_destroy(ids, current_user)
    Order.destroy ids
  end

  #metodos para integración con mercadopago
  def create_preference
    if self.status_id_changed? and self.status_id == 2 and self.created_at? and self.has_payment? :credito
      preference = PayMercadoPago::MP.create_preference self.preferenceData
      pay = PaymentReference.new params_payment_reference(preference)
      pay.save
      self.payment_reference_id = pay.id
    #else
      #preference = PayMercadoPago::MP.update_preference self.payment_reference.preference_id, self.preferenceData
      #self.payment_reference.update_attributes params_payment_reference(preference)
    end
  end

  #obtener preferencia de pago
  def get_preference
    PayMercadoPago::MP.get_preference(self.payment_reference.preference_id)
  end


  #crear data de la preferencia de pago
  def preferenceData
    { items: [{title: "Orden ##{self.id}", quantity: 1, unit_price: self.price_order_total.round(2), currency_id: "VEF" }], payer: { email: self.user.email, name: self.user.name, identification: { number: self.user.identification }}, payment_methods: { excluded_payment_types: [{id: 'atm'}, {id: "ticket"}]}}
  end

  #verificar si el pago ha sido aprobado
  def pay_approved?
    self.payment_statuses.last.status == "approved" unless self.payment_statuses.empty?
  end

  #verficar si el pago se permite pagar
  def permit_pay?
     self.payment_statuses.empty? ? true : (!["approved", "pending", "in_process"].include? self.payment_statuses.last.status) 
  end

  #obtener el ultimo status del pago
  def last_message_payment_status
    self.payment_statuses.empty? ? "Aún no se registran pagos" : messages_status_payment(self.payment_statuses.last.status)
  end

  def number_of_transfer
    self.payment_statuses.empty? ? "Aún no se registra un codigo de referencia" : self.payment_statuses.last.payment_id
  end

  def cash_confirm
    self.payment_statuses.empty? ? "Aún no se confirma la orden." : "La orden ha sido confirmada."
  end

  private

  #obtener mensajes segun el status de pago
  def messages_status_payment(key)
    messages = { "approved" => "El pago fue aprobado y acreditado.", "pending" => "El usuario no completó el proceso de pago.", "in_process" => "El pago está siendo revisado.", "rejected" => "El pago fue rechazado. El usuario puede intentar nuevamente.", "refunded" => "El pago fue devuelto al usuario.", "cancelled" => "El pago fue cancelado por superar el tiempo necesario para realizar el pago o por una de las partes.", "in_mediation" => "Se inició una disputa para el pago.", "charged_back" => "Se realizó un contracargo en la tarjeta de crédito." }
    messages[key]
  end

  def assign_status
    self.status_id = 1 unless self.created_at?
  end   

  def assign_price_delivery
    self.price_delivery = 0 unless self.created_at?
  end

  def delete_items
    Item.delete self.items.ids
  end

  #dar formato al hash de la preferencia de pago
  def params_payment_reference(preference)
    preference = preference["response"].as_json(only: ["collector_id", "id", "date_created", "init_point", "sandbox_init_point"])
    preference["collection_id"], preference["preference_id"] = preference.delete("collector_id"), preference.delete("id")
    preference
  end
end

Order.import
