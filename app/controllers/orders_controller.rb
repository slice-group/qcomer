class OrdersController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_order, only: [:show, :edit, :update, :destroy]


  def index
    model = params[:controller].classify.constantize;

    if params[:search].blank?
      @objects = model.index(current_user)
      @total_rows = @objects.count
      @objects = last_page(@objects, params[:currentPage].to_i) if @objects.count != 0
    else
      if(params[:direction]=="refresh")
        sleep(1)
      end
      items = model.search(model.query params[:search])
      @total_rows = model.index_total items.records, current_user
      @objects = model.index_search last_page_search(items, params[:currentPage].to_i).records, current_user
    end
    
    respond_to do |format|
      format.html { render action: 'index' }
      format.json { render :json => { objects: @objects.as_json(only: [:id, :address], include: [{ user: { only: [:name, :phone, :mobile] }}, {payment: { only: [:id, :name] }}, {status: {only: [:name, :id]}}], methods: [:price_dishes_total, :items_count, :last_message_payment_status, :number_of_transfer, :cash_confirm, :price_order_total]), total_rows: @total_rows } }
    end
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/1/edit
  def edit 
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Orden actualizada satisfactoriamente' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:user_id, :status_id, :address, :observation, :price_delivery, :payment_id, items_attributes: [:id, :dish_id, :quantity, :_destroy])
    end
end
