class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  layout :layout_by_resource
  before_filter :configure_permitted_parameters, if: :devise_controller?
  respond_to :html, :json
  skip_before_filter :verify_authenticity_token, if: :devise_controller?  
  
  #adaptacion para que cancan no bloquee los methods_params de los controladores
  before_filter do
    resource = controller_name.singularize.to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end
  
  rescue_from CanCan::AccessDenied do |exception|
    route = exception.subject.methods.include?(:route_index) ? main_app.admin_index_path : exception.subject.class.to_s.classify.constantize.route_index
    redirect_to route, :alert => "#{exception.action}: #{exception.message}"
  end

  def index
    model = params[:controller].classify.constantize;

    if params[:search].blank?
      @objects = model.index(current_user)
      @total_rows = @objects.count
      @objects = last_page(@objects, params[:currentPage].to_i) if @objects.count != 0
    else
      if(params[:direction]=="refresh")
        sleep(1)
      end
      items = model.search(model.query params[:search])
      @total_rows = model.index_total items.records, current_user
      @objects = model.index_search last_page_search(items, params[:currentPage].to_i).records, current_user
    end
    
    respond_to do |format|
      format.html { render action: 'index' }
      format.json { render :json => { objects: @objects, total_rows: @total_rows } }
    end
  end

  def delete
    model = params[:controller].classify.constantize;

    ids = redefine_destroy params[:ids].split(","), model 
    
    model.multiple_destroy ids, current_user #delets items

    respond_to do |format|
      format.json { render :json => { total: model.count }  }
    end
  end

  protected

  #metodo para redefinir el array de los elementos selecionados que se van a eliminar
  def redefine_destroy(ids, model)
    ids.sort.each do |id|
      ids.delete id unless model.exists? id
    end
    ids
  end

  def last_page(objects, page)
    if objects.page(page).exists?
      objects.page(page)
    else 
      return last_page(objects, page-1)
    end
  end

  def last_page_search(objects, page)
    if page <= objects.total_pages
      objects.page(page)
    else 
      return last_page_search(objects, page-1)
    end
  end
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:name, :email, :password, :password_confirmation)
    end
    devise_parameter_sanitizer.for(:account_update) do |u|
      u.permit(:name, :email, :password, :password_confirmation, :current_password)
    end
  end

  def layout_by_resource
    if devise_controller? 
      "admin/application"
    else
      "application"
    end
  end

  #authentication token
  def auth_token!
    user = User.find_by_authentication_token(request.headers["X-auth-token"])
    @current_user = user
    unless user
      return render :json=> { :success=>false, :message=>"Debes autenticarte para poder tener acceso."}, status: 200
    end
  end

end
