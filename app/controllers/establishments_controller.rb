class EstablishmentsController < ApplicationController
  before_action :set_establishment, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  respond_to :html

  def show
    respond_with(@establishment)
  end

  def new
    @establishment = Establishment.new
  end

  def edit
  end

  def create
    @establishment = Establishment.new(establishment_params)
    @establishment.save
    respond_with(@establishment)
  end

  def update
    @establishment.update(establishment_params)
    @establishment.index_elasticsearch_document
    respond_with(@establishment)
  end

  def destroy
    @establishment.destroy
    respond_with(@establishment)
  end

  private
    def set_establishment
      @establishment = Establishment.find(params[:id])
    end

    def establishment_params
      params.require(:establishment).permit(:brand, :banner, :name, :description, :permalink, :public, :credit_card, :debit, :cash, :address, :city, :state, :country, :phone_1, :phone_2, :facebook_name, :instagram_name, :twitter_name, :latitude, :longitude, :day_aperture, :day_close, :hour_aperture, :hour_close, :public_landing, :delivery, {:category_ids => []}, {:service_ids => []})
    end
end
