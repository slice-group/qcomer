class FrontendController < ApplicationController
	layout 'layouts/frontend/application'
  def index
  	Location.add(request.remote_ip)
    @establishments = Establishment.where(public_landing: true).order("created_at DESC").limit(12)  
  end

  def establishments
    Location.add(request.remote_ip)
    @categories = Category.all
  	unless params[:search].blank?		
  		@establishments = Establishment.search(Establishment.query_frontend(params[:search])).records  		
  	else
      @establishments = Establishment.all
  	end
  end

  def search_by_category
    Location.add(request.remote_ip)
    @establishments = Category.find_by_permalink(params[:permalink]).establishments
    @search = params[:permalink]
    @categories = Category.all
  end

  def establishment_profile
    Location.add(request.remote_ip)
    @establishment = Establishment.find_by_permalink(params[:permalink])
  end
end
