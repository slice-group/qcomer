class ApiUserController < ApplicationController
	#before_filter :authenticate_user!
	before_filter :auth_token! #bloqea el acceso a usuario no logeados, retorna @current_user
	skip_before_filter :verify_authenticity_token

	respond_to :json

	def edit
		if @current_user.update_attributes(user_params)
			render :json=> { success: true, :user=> @current_user.as_json, message: "Datos salvados" }, status: 200
		else
			render :json=> { success: false, :errors=> @current_user.errors }, status: 200
		end
	end

	def edit_password
		if @current_user.valid_password? user_params_password[:actual_password]
			if @current_user.update_attributes(password: user_params_password[:password], password_confirmation: user_params_password[:password_confirmation]) 
				render :json=> { success: "CORRECT", message: "Su password ha sido modificado" }, status: 200
			else
			 render :json=> { success: "ERRORS", :errors=> @current_user.errors }, status: 200
			end
		else
			render :json=> { success: "INCORRECT", message: "Su password actual no coincide" }, status: 200
		end
	end

	private

  def user_params
    params.require(:user).permit(:name, :email, :identification, :address, :phone, :mobile, :password, :password_confirmation, :encrypted_password)
  end

  def user_params_password
    params.require(:user).permit(:actual_password, :password, :password_confirmation, :encrypted_password)
  end
end
