class DishesController < ApplicationController
  before_action :set_dish, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  respond_to :html

  def index
    @establishment = Establishment.find(params[:establishment_id])

    model = params[:controller].classify.constantize;

    if params[:search].blank?
      @objects = model.index(current_user, @establishment.id)
      @total_rows = @objects.count
      @objects = last_page(@objects, params[:currentPage].to_i) if @objects.count != 0
    else
      if(params[:direction]=="refresh")
        sleep(1)
      end
      items = model.search(model.query params[:search])
      @total_rows = model.index_total items.records, current_user, @establishment.id
      @objects = model.index_search last_page_search(items, params[:currentPage].to_i).records, current_user, @establishment.id
    end
    
    respond_to do |format|
      format.html { render action: 'index' }
      format.json { render :json => { objects: @objects, total_rows: @total_rows } }
    end
  end

  def show
  end

  def new
    @dish = Dish.new
  end

  
  def edit
  end


  def create
    @dish = Dish.new(dish_params)

    respond_to do |format|
      if @dish.save
        format.html { redirect_to establishment_dish_path(@dish.establishment_id, @dish.id), notice: 'El plato ha sido creado satisfactoriamente.' }
        format.json { render :show, status: :created, location: @dish }
      else
        format.html { render :new }
        format.json { render json: @dish.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dishes/1
  # PATCH/PUT /dishes/1.json
  def update
    respond_to do |format|
      if @dish.update(dish_params)
        @dish.index_elasticsearch_document
        format.html { redirect_to establishment_dish_path(@dish.establishment_id, @dish.id), notice: 'El plato ha sido actualizado satisfactoriamente.' }
        format.json { render :show, status: :ok, location: @dish }
      else
        format.html { render :edit }
        format.json { render json: @dish.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dishes/1
  # DELETE /dishes/1.json
  def destroy
    @dish.destroy
    respond_to do |format|
      format.html { redirect_to establishment_dishes_path, notice: 'El plato ha sido eliminado satisfactoriamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dish
      @dish = Dish.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dish_params
      params.require(:dish).permit(:name, :description, :price, :delivery_time, :image, :permalink, :establishment_id, {:category_ids => []})
    end
end
