class ApiController < ApplicationController
	skip_before_filter :verify_authenticity_token  
	#before_filter :authenticate_user!
	before_filter :auth_token!, only: [:create_order, :index_orders, :canceled_order, :pay_order]

	respond_to :json

	def establishments
		@establishments = Establishment.where(delivery: true).sample(8)
		render :json=> { :establishments => @establishments.as_json }, status: 200 
	end

	def establishment_perfil
		@establishment = Establishment.find_by_permalink(params[:permalink])
		render :json=> { :establishment => @establishment.as_json }, status: 200
	end

	def dishes_the_establishment
		@dishes = Establishment.find_by_permalink(params[:permalink]).dishes

		#ordenamiento
		order = params[:order].blank? ? "id" : "price #{params[:order]}"

		if @dishes.count.zero?
			render :json=> { :message => "Este establecimiento aún no tiene menú." }, status: 200
		else
			render :json=> { totalItems: @dishes.count, :dishes => @dishes.order(order).page(params[:current_page]).per(10).as_json(only: [:id, :name, :image, :description, :permalink, :price, :delivery_time], include: [{establishment: {only: [:name, :permalink], methods: [:open]}}] ) }, status: 200
		end
	end

	def search
		@dishes = Dish.search(Dish.query_frontend params[:query], params[:order]) 

		#ordenamiento
		order = params[:order].blank? ? "id" : "price #{params[:order]}"

		if @dishes.count.zero?
			render :json=> { :message => "No se encontraron resultados de búsqueda." }, status: 200
		else
			render :json=> { totalItems: @dishes.count, :dishes => @dishes.records.order(order).page(params[:current_page]).per(10).as_json(only: [:id, :name, :image, :description, :permalink, :price, :delivery_time], include: [{establishment: {only: [:name, :permalink], methods: [:open]}}] ) }, status: 200
		end
	end

	def categories
		@categories = Category.order("name DESC")
		render :json=> { :categories => @categories }, status: 200 
	end


	#manage order
	def create_order
		order = Order.new order_params.merge! user_id: @current_user.id
		if order.save
			render :json=> { success: true, :message => "Tu orden fue enviada correctamente, espere unos minutos mientras validamos el pedido." }, status: 200 
		else
			render :json=> { success: false, :errors => order.errors.messages.map { |k, v| "#{t('activerecord.attributes.order.'+k.to_s)}: #{v.first}" } }, status: 200
		end
	end

	#api/orders
	def index_orders
		orders = Order.where(user_id: @current_user.id).order("created_at DESC")
		render :json=> { totalItems: orders.count, :orders => orders.page(params[:current_page]).per(10).as_json(only: [:id, :address, :price_delivery, :created_at], include: [{status: {only: [:id, :name]}}], methods: :price_dishes_total) }
	end

	def canceled_order
		order = Order.find params[:id]
		order.update_attributes status_id: 6
		render :json=> { :order => order.as_json(only: [:id, :address, :total_price, :created_at], include: [{status: {only: [:id, :name]}}], methods: :price_dishes_total), message: "La orden ##{order.id} fue cancelada." }, status: 200
	end

	def pay_order
		order = Order.find params[:id]
		render :json=> { :order => order.as_json(only: [:id, :address, :total_price, :created_at, :price_delivery, :observation], include: [{status: {only: [:id, :name]}}, {payment: {only: [:id, :name]}}, {items: {only: [:quantity], methods: [:price_total], include: {dish: { only: [:id, :name, :description, :price, :delivery_time, :image, :permalink], include: {establishment: {only: [:name, :permalink]}}}}}}, {payment_reference: {only: [:sandbox_init_point]}}], methods: [:price_dishes_total, :price_order_total, :sandbox_init_point])}, status: 200
	end


	#recive notificaciones solo para transferenciasy efectivo
	def payment_notifications
		order = Order.find params[:order_id]
		
		#notificaciones por el modal
		if order.has_payment? :credito
			#PaymentStatus.create_payment_status(params[:id])
			render json: { status: "200", message: "Espere mientras confirmamos su pago.", order: order.as_json(only: [:id], include: [{status: {only: [:id, :name]}}])  }
		end

		if order.has_payment? :transferencia
			payment = PaymentStatus.new(payment_id: params[:id], reason: "Transferencia", order_id: order.id, transaction_amount: order.price_order_total)
			if payment.save
				order.update_attributes status_id: 3 
				render json: { status: "200", success: true, message: "Espere mientras confirmamos su transferencia.", order: order.as_json(only: [:id], include: [{status: {only: [:id, :name]}}]) }
			else
				render json: { status: "200", success: false, errors: payment.errors.as_json }
			end
		end

		if order.has_payment? :efectivo
			payment = PaymentStatus.new(payment_id: "N/A", reason: "Efectivo", order_id: order.id, transaction_amount: order.price_order_total)
			order.update_attributes status_id: 4 if payment.save
			render json: { status: "200", success: true, message: "Ya su orden se esta en camino.", order: order.as_json(only: [:id], include: [{status: {only: [:id, :name]}}]) }
		end
	end


	#notificaciones para tarjeta de credito con mercado pago
	def payment_notifications_mp
		if params[:topic] == "payment"
			PaymentStatus.create_payment_status(params[:id])
			render status: 200, json: { status: "200" }
		else
			render status: 380, json: { message: "notificacion no recibida" }
		end
		
	end

	private

	def order_params
		params.require(:order).permit(:address, :payment_id, items_attributes: [:dish_id, :quantity, :observation])
  end

end
