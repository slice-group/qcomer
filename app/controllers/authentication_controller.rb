class AuthenticationController < ApplicationController
  # Turn off user authentication for all actions inauthenticate?email=luis.prz7@gmail.com&password=12345678 this controller
  skip_before_filter :verify_authenticity_token

  def login
		resource = User.find_for_database_authentication(email: params[:email])

		if resource and resource.valid_password?(params[:password]) 
			sign_in(resource, store:false)
			render :json=> { success: true, :user=> current_user.as_json, message: "Has ingresado correctamente" }, status: 200
		else 
			render :json=> { success: false, :message=>"Error, verifica tu email o password"}, status: 200
  	end
  end

  def register
		resource = User.new(name: params[:name], email: params[:email], password: params[:password], password_confirmation: params[:password_confirmation], role_ids: 4)

		if resource.save
			render :json=> { success: true, :user => resource.as_json, message: "#{resource.name} has sido registrado correctamente" }, status: 200
		else
			render :json=> { success: false, :errors=> resource.errors.messages }, status: 200
		end
  end

  def destroy
  	sign_out(current_user)
  	render :json=> { success: true, :message => "Has salido del sistema" }, status: 200
  end

  def forgot_password
  	resource = User.find_by_email(params[:email])

  	if resource 
  		resource.api_send_reset_password_instructions
  		render :json=> { success: true, :message => "Ha sido enviado un correo a tu email, sigue las instrucciones para reestablecer tu contraseña", reset_password_token: resource.reset_password_token }, status: 200
  	else
  		render :json=> { success: false, :message => "Tu email no se encuentra registrado." }, status: 200
  	end
  end

  def reset_password
  	resource = User.find_by_reset_password_token(params[:reset_password_token])

  	if resource
  		if resource.reset_password(params[:password], params[:password_confirmation])
  			render :json=> { success: "CORRECT", :message => "Tu password ha sido cambiado correctamente", email: resource.email }, status: 200
  		else
  			render :json=> { success: "ERRORS", :errors => resource.errors }, status: 200
  		end
  	else
  		render :json=> { success: "NOTOKEN", :message => "Debe solicitar nuevamente reestablecer su contraseña" }, status: 200
  	end
  end
end