class AuthenticationMailer < ApplicationMailer
	def reset_password_instructions(user)
		@user = user
		mail(to: @user.email, subject: 'Te lo llevo paraguaná')
	end
end
