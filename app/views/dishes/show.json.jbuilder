json.extract! @dish, :id, :name, :description, :price, :delivery_time, :image, :permalink, :created_at, :updated_at
