json.array!(@dishes) do |dish|
  json.extract! dish, :id, :name, :description, :price, :delivery_time, :image, :permalink
  json.url dish_url(dish, format: :json)
end
