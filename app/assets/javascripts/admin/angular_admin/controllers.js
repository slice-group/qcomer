angular.module('admin', [])
  .controller('indexObjectCtrl', ['$scope','model', function($scope, model) {
			
		$scope.btnDelete = false;		
		$scope.currentPage = 0;
		$scope.currentPageServer = 1;
		$scope.currentPageRequest = 0;
  		$scope.pagesItemsSelected = {}
		//Alerts
		$scope.alert = false;
		$scope.statusAlert = "";
		$scope.contentAlert = ""
		$scope.codAlert = ""

		// routes path
		$scope.update_path = function (id) {
		  return $scope.route_path+"/"+id
		};
		$scope.edit_path = function (id) {
		  return $scope.route_path+"/"+id+"/edit";
		};

		$scope.destroy_path = function (id) {
		  return $scope.route_path+"/"+id;
		};

		$scope.show_path = function (id) {
		  return $scope.route_path+"/"+id;
		};
		// --------------------

		$scope.init = function(json, route_path, search, total_objects, pageSize){
			$scope.pageSize = pageSize;
			$scope.lengthPage = pageSize;
			$scope.pagesItemsSelected[$scope.currentPage+1] = [];
			$scope.objects = json;
			$scope.route_path = route_path;
			$scope.total_objects = total_objects;
			$scope.search = search;
			$scope.show_search(true);
		}

		$scope.show_search = function(init){
			if(init==true){
				if($('.input-search').val()){
					$('.box-search').css('top','0px');

				}
			}else{					
				$('.box-search').animate({
					top:"0px"
				}, 200);
			}
		};

		$scope.hide_search = function(){
			$('.box-search').animate({
				top:"-50px"
			}, 200);
			$scope.search = "";
			$(".input-search").val("")
		};

		$scope.destroy = function(message) {	
		 	if (confirm(message) == true) {						
			 	$scope.loadingDelete = true;
			 	model.destroy($scope);
			}
		};

		$scope.show_object = function(object) {
			$scope.object_data = object
			$("tbody").find(".current").removeClass("current")
			$("#row-"+$scope.object_data.id).addClass('current')
		}

		$scope.selected = function(object){
			ctrl.itemSelected(object, $scope);
		};

		$scope.allSelected = function(){
			($scope.btnDelete) ? ctrl.allItemRemoveSelected($scope) : ctrl.allItemsSelected($scope)
		};

		$scope.refresh = function(){	
			model.refresh_page($scope, "refresh")
			$scope.object_data = null;
		};

		$scope.alertClose = function(){
			$scope.alert = false;
		}

		$scope.numberOfPages=function(){
	    	return Math.ceil($scope.total_objects/$scope.pageSize);                
	  	}

	    //paginator
	    $scope.next = function() {
	    	model.next_page($scope, "next")
	    }

	    $scope.last = function() {
	    	model.last_page($scope, "last")
	    }

	    $scope.isLastPage = function() {
			if ($scope.currentPageServer == $scope.numberOfPages()) {
				return true
			} else {
				return false
			}
		}

		$scope.isFirstPage = function() {
			if($scope.currentPageServer == 1) {
				return true
			} else {
				return false
			}
		}

		$scope.calculateLengthPage = function () {
			if (!$scope.isLastPage()) {
				return $scope.pageSize;
			}	else {
				if($scope.numberOfPages() == 1) { index=0 } else { index=1 }
				length = $scope.total_objects%(($scope.numberOfPages()-index)*$scope.pageSize)
				if (length == 0) { return $scope.pageSize } else { return length }
			}
		}


		$scope.activeAlerts = function(){
			$(".alert").removeClass("removeAlert");
			$(".alert").addClass("addAlert");
		}
	}]);

  

