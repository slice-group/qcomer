$(document).ready(function(){
	$("#home").css("height", $(window).height());
	$(".responsive-container ").css("height", $(window).height());
	$(".img-container ").css("height", $(window).height());
	$(".dummy").css("height", $(window).height());
	$(window).resize(function() {	  
		$("#home").css("height", $(window).height());
		$(".img-container ").css("height", $(window).height());
		$(".responsive-container ").css("height", $(window).height());
		$(".dummy").css("height", $(window).height());
	});

	function removeHash () { 
	  history.pushState("", document.title, window.location.pathname + window.location.search);
	}

	$('.top-nav').onePageNav({
	    currentClass: 'current',
	    changeHash: false,
	    scrollSpeed: 500,
	    scrollThreshold: 0.5,
	    filter: ':not(.external)',
	    easing: 'swing',
	    begin: function() {

	    },
	    end: function() {   

	    },
	    scrollChange: function($currentListItem) {      
	      		  
	    }
	});

	$('.image-home').animate({
		opacity: 1
	}, 2000);

});